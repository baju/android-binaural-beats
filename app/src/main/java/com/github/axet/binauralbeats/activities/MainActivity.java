package com.github.axet.binauralbeats.activities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.axet.androidlibrary.animations.RemoveItemAnimation;
import com.github.axet.androidlibrary.widgets.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.AppCompatThemeActivity;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.animations.BeatsAnimation;
import com.github.axet.binauralbeats.app.MainApplication;
import com.github.axet.binauralbeats.beats.BeatsPlayer;
import com.github.axet.binauralbeats.beats.BinauralBeatVoice;
import com.github.axet.binauralbeats.beats.DefaultProgramsBuilder;
import com.github.axet.binauralbeats.beats.Note;
import com.github.axet.binauralbeats.beats.Period;
import com.github.axet.binauralbeats.beats.Program;
import com.github.axet.binauralbeats.beats.ProgramMeta;
import com.github.axet.binauralbeats.beats.SoundLoop;
import com.github.axet.binauralbeats.services.BeatsService;
import com.github.axet.binauralbeats.widgets.CustomDialog;
import com.github.axet.binauralbeats.widgets.LineGraphView;
import com.github.axet.binauralbeats.widgets.OpenDialog;
import com.github.axet.binauralbeats.widgets.SoundTestDialog;

import java.lang.reflect.Method;
import java.util.HashMap;

public class MainActivity extends AppCompatThemeActivity implements AbsListView.OnScrollListener, DialogInterface.OnDismissListener {
    public final static String TAG = MainActivity.class.getSimpleName();

    public final static String STOP_BUTTON = MainActivity.class.getCanonicalName() + ".STOP_BUTTON";
    public final static String PAUSE_BUTTON = MainActivity.class.getCanonicalName() + ".PAUSE_BUTTON";

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};

    public static final int RESULT_FILE = 1;

    static final int TYPE_COLLAPSED = 0;
    static final int TYPE_EXPANDED = 1;
    static final int TYPE_DELETED = 2;

    public static final int PLAYING_CUSTOM = -2;
    public static final int PLAYING_NONE = -1;

    public static final int PLAYER_UPDATE = 1000;

    class PhoneStateChangeListener extends PhoneStateListener {
        public boolean wasRinging;
        public boolean pausedByCall;

        @Override
        public void onCallStateChanged(int s, String incomingNumber) {
            switch (s) {
                case TelephonyManager.CALL_STATE_RINGING:
                    wasRinging = true;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    wasRinging = true;
                    if (recordings.player != null && recordings.player.isPlaying() && !recordings.player.isEnd()) {
                        recordings.playerPause();
                        pausedByCall = true;
                    }
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    if (pausedByCall) {
                        recordings.playerPause();
                    }
                    wasRinging = false;
                    pausedByCall = false;
                    break;
            }
        }
    }

    final int[] ALL = {TYPE_COLLAPSED, TYPE_EXPANDED};

    int scrollState;

    Recordings recordings;
    ListView list;
    Handler handler;

    View fab_panel;
    View fab_stop;
    FloatingActionButton fab;

    BroadcastReceiver receiver;
    ScreenReceiver screenreceiver;
    PhoneStateChangeListener pscl = new PhoneStateChangeListener();

    OpenChoicer choicer;

    public static void startActivity(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public class Recordings extends ArrayAdapter<Object> {

        BeatsPlayer player;

        int playing;
        View playingView; // current playing view

        Runnable updatePlayer;
        int selected = PLAYING_NONE;
        ProgramMeta.Category old;

        HashMap<ProgramMeta, Program> library = new HashMap<>();
        HashMap<ProgramMeta.Category, String> cats = new HashMap<>();

        public Recordings(Context context) {
            super(context, 0);
        }

        public void scan() {
            setNotifyOnChange(false);
            clear();

            load(ProgramMeta.Category.HYPNOSIS, R.string.group_hypnosis);
            load(ProgramMeta.Category.SLEEP, R.string.group_sleep);
            load(ProgramMeta.Category.HEALING, R.string.group_healing);
            load(ProgramMeta.Category.LEARNING, R.string.group_learning);
            load(ProgramMeta.Category.MEDITATION, R.string.group_mediation);
            load(ProgramMeta.Category.STIMULATION, R.string.group_simulation);
            load(ProgramMeta.Category.OOBE, R.string.group_oobe);
            load(ProgramMeta.Category.OTHER);

            load(R.string.program_morphine, "HEALING_MORPHINE");
            load(R.string.program_learning, "LEARNING_LEARNING");
            load(R.string.program_wakefulrelax, "MEDITATION_WAKEFULRELAX");
            load(R.string.program_schumann_resonance, "MEDITATION_SCHUMANN_RESONANCE");
            load(R.string.program_unity, "MEDITATION_UNITY");
            load(R.string.program_shamanic_rhythm, "MEDITATION_SHAMANIC_RHYTHM");
            load(R.string.program_powernap, "SLEEP_POWERNAP");
            load(R.string.program_sleep_induction, "SLEEP_SLEEP_INDUCTION");
            load(R.string.program_smr, "SLEEP_SMR");
            load(R.string.program_airplanetravelaid, "SLEEP_AIRPLANETRAVELAID");
            load(R.string.program_adhd, "STIMULATION_ADHD");
            load(R.string.program_hiit, "STIMULATION_HIIT");
            load(R.string.program_hallucination, "STIMULATION_HALLUCINATION");
            load(R.string.program_highest_mental_activity, "STIMULATION_HIGHEST_MENTAL_ACTIVITY");
            load(R.string.program_creativity, "STIMULATION_CREATIVITY");
            load(R.string.program_self_hypnosis, "HYPNOSIS_SELF_HYPNOSIS");
            load(R.string.program_lucid_dreams, "OOBE_LUCID_DREAMS");
            load(R.string.program_astral_01_relax, "OOBE_ASTRAL_01_RELAX");
            load(R.string.program_lucid_dreams_2, "OOBE_LUCID_DREAMS_2");

            notifyDataSetChanged();
        }

        void load(ProgramMeta.Category c) {
            cats.put(c, c.toString().toUpperCase());
        }

        void load(ProgramMeta.Category c, int id) {
            String n = getString(id);
            cats.put(c, n.toUpperCase());
        }

        void load(int id, String name) {
            String desc = getString(id);
            String[] ss = desc.split("\\|");
            Program p = new Program(ss[0]);
            Method m;
            try {
                m = DefaultProgramsBuilder.class.getDeclaredMethod(name, Program.class);
                p = (Program) m.invoke(null, p);
            } catch (NoSuchMethodException e) {
                try {
                    m = DefaultProgramsBuilder.class.getDeclaredMethod(name, Context.class, Program.class);
                    p = (Program) m.invoke(null, MainActivity.this, p);
                } catch (Exception ee) {
                    Log.d(TAG, "Load error", ee);
                    return;
                }
            } catch (Exception e) {
                Log.d(TAG, "Load error", e);
                return;
            }
            if (ss.length > 1)
                p.setDescription(ss[1]);
            ProgramMeta.Category c = DefaultProgramsBuilder.getMatchingCategory(name);
            ProgramMeta pm = new ProgramMeta(m, p.getName(), c);
            library.put(pm, p);
            if (old != c) {
                old = c;
                add(cats.get(c));
            }
            add(pm);
        }

        public void close() {
            playerStop();
            if (player != null) {
                player.release();
                player = null;
            }
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            Object o = getItem(position);

            if (convertView == null) {
                if (o instanceof String) {
                    convertView = inflater.inflate(R.layout.category, parent, false);
                } else {
                    convertView = inflater.inflate(R.layout.recording, parent, false);
                    convertView.setTag(-1);
                }
            }

            if (o instanceof String) {
                TextView t = (TextView) convertView.findViewById(R.id.category_text);
                t.setText((String) o);
                return convertView;
            }

            final View fview = convertView;
            final View base = convertView.findViewById(R.id.recording_base);

            if (playing == position) {
                playingView = convertView;
            } else { // different index
                if (playingView == convertView) // reused view
                    playingView = null;
            }

            if ((int) convertView.getTag() == TYPE_DELETED) {
                RemoveItemAnimation.restore(base);
                convertView.setTag(-1);
            }

            final ProgramMeta pm = (ProgramMeta) o;
            final Program pr = library.get(pm);

            TextView title = (TextView) convertView.findViewById(R.id.recording_title);
            title.setText(pm.getName());

            TextView desc = (TextView) convertView.findViewById(R.id.recording_desc);
            desc.setText(pr.getDescription() + " " + pr.getAuthor());

            final Runnable pl = new Runnable() {
                @Override
                public void run() {
                    if (player == null) {
                        playerPlay(fview, pr, position);
                    } else if (player.isPlaying()) {
                        if (playing != position) {
                            playerStop();
                            notifyDataSetChanged(); // update previous player View
                            playerPlay(fview, pr, position);
                        } else {
                            playerPause(fview, pr, position);
                        }
                    } else {
                        if (playing != position) {
                            playerStop();
                            notifyDataSetChanged(); // update previous player View
                        }
                        playerPlay(fview, pr, position);
                    }
                    updateCustom();
                }
            };

            View ctrl = convertView.findViewById(R.id.beats_controls);

            final View play = convertView.findViewById(R.id.beats_player_play);
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pl.run();
                }
            });
            final View play2 = ctrl.findViewById(R.id.beats_player_play);
            play2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pl.run();
                }
            });

            final Runnable st = new Runnable() {
                @Override
                public void run() {
                    playerStop();
                    notifyDataSetChanged(); // updatePlayerText(fview, pr, position); // does not update ctrls visibility
                }
            };

            final View stop = convertView.findViewById(R.id.beats_player_stop);
            stop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    st.run();
                }
            });
            final View stop2 = ctrl.findViewById(R.id.beats_player_stop);
            stop2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    st.run();
                }
            });

            updatePlayerText(fview, pr, position);

            final View playerBase = convertView.findViewById(R.id.recording_player);
            playerBase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            if (selected == position) {
                BeatsAnimation.apply(list, convertView, true, scrollState == SCROLL_STATE_IDLE && (int) convertView.getTag() == TYPE_COLLAPSED, Recordings.this.playing == position);
                convertView.setTag(TYPE_EXPANDED);

                updatePlayerText(fview, pr, position);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(PLAYING_NONE);
                    }
                });

                TextView desc2 = (TextView) convertView.findViewById(R.id.recording_desc2);
                desc2.setText(pr.getDescription() + " " + pr.getAuthor());
                desc2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(PLAYING_NONE);
                    }
                });
            } else {
                BeatsAnimation.apply(list, convertView, false, scrollState == SCROLL_STATE_IDLE && (int) convertView.getTag() == TYPE_EXPANDED, Recordings.this.playing == position);
                convertView.setTag(TYPE_COLLAPSED);

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        select(position);
                    }
                });
            }

            return convertView;
        }

        void playerPlay(View v, Program pr, int position) {
            if (player != null && player.isEnd()) {
                playerStop();
            }
            if (player == null) {
                player = new BeatsPlayer(getContext(), pr);
            }
            player.setFlatVcVol(MainApplication.getFlatVcVol(MainActivity.this));
            player.setFlatBgVol(MainApplication.getFlatBgVol(MainActivity.this));

            player.play();

            Recordings.this.playing = position;
            Recordings.this.playingView = v;

            updatePlayerRun(pr, position);

            BeatsService.startService(getContext(), pr.getName(), player.getStatus(), true, false);
        }

        void playerPause() {
            if (player == null)
                return;
            if (!player.isPlaying()) {
                playerPlay(playingView, player.getProgram(), playing);
            } else {
                playerPause(playingView, player.getProgram(), playing);
            }
            notifyDataSetChanged();
        }

        void playerPause(View v, Program pr, int position) {
            BeatsService.startService(getContext(), pr.getName(), player.getStatus(), false, false);
            if (player != null) {
                player.pause();
            }
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
            if (v != null)
                updatePlayerText(v, pr, position);
        }

        void playerStop() {
            BeatsService.stopService(getContext());
            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }
            if (player != null) {
                player.release();
                player = null;
            }
            playing = PLAYING_NONE;
        }

        void updatePlayerRun(final Program f, final int position) {
            if (playingView != null) { // prevent reuse scroll / update view
                updatePlayerText(playingView, f, position);
            }

            boolean playing = player != null && player.isPlaying();
            boolean end = player == null || player.isEnd();
            BeatsService.startService(getContext(), f.getName(), player != null ? player.getStatus() : "", playing, end);

            if (updatePlayer != null) {
                handler.removeCallbacks(updatePlayer);
                updatePlayer = null;
            }

            if (!playing) {
                updateCustom();
                return;
            }

            updatePlayer = new Runnable() {
                @Override
                public void run() {
                    updatePlayerRun(f, position);
                }
            };
            handler.postDelayed(updatePlayer, PLAYER_UPDATE);
        }

        boolean updatePlayerText(final View v, final Program pr, int position) {
            boolean playing = false;

            long c = 0;
            String s = "";

            if (player != null && Recordings.this.playing == position) {
                playing = player.isPlaying();
                c = player.getCurrentPosition();
                s = player.getStatus();
            }

            ImageView i = (ImageView) v.findViewById(R.id.beats_player_play);
            if (i != null)
                i.setImageResource(playing ? R.drawable.pause : R.drawable.play);
            View ctrls = v.findViewById(R.id.beats_controls);
            if (ctrls != null) { // update status bar controls
                i = (ImageView) ctrls.findViewById(R.id.beats_player_play);
                if (i != null)
                    i.setImageResource(playing ? R.drawable.pause : R.drawable.play);
            }
            final View stop = v.findViewById(R.id.beats_player_stop);
            if (stop != null) // custom beats
                stop.setVisibility(player != null ? View.VISIBLE : View.INVISIBLE);

            LineGraphView bar = (LineGraphView) v.findViewById(R.id.recording_player_seek);
            if (bar != null) {
                bar.load(pr);
                bar.setDrawBackground(true);
                bar.setDrawBackgroundLimit(c);
                if (playing) {
                    bar.setViewPort(0, pr.getLength());
                }
                bar.invalidate();
            }

            TextView status = (TextView) v.findViewById(R.id.recording_player_status);
            if (s.isEmpty()) {
                s = MainApplication.formatDuration(getContext(), pr.getLength() * 1000);
            }
            status.setText(s);

            return playing;
        }

        public void select(int pos) {
            selected = pos;
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            Object o = getItem(position);
            if (o instanceof String)
                return 1;
            return super.getItemViewType(position);
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }
    }

    @Override
    public int getAppTheme() {
        return MainApplication.getTheme(this, R.style.AppThemeLight_NoActionBar, R.style.AppThemeDark_NoActionBar);
    }

    public static float getFloat(SharedPreferences shared, String key, float def) { // < 1.1.9
        try {
            return shared.getInt(key, (int) def);
        } catch (ClassCastException c) {
            return shared.getFloat(key, def);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String a = intent.getAction();
                if (a == null)
                    return;
                if (a.equals(PAUSE_BUTTON)) {
                    if (recordings.playing == PLAYING_CUSTOM) {
                        if (recordings.player.isEnd()) {
                            playCustom();
                        } else {
                            recordings.playerPause();
                            updateCustom();
                        }
                    } else {
                        recordings.playerPause();
                        updateCustom();
                    }
                }
                if (a.equals(STOP_BUTTON)) {
                    recordings.playerStop();
                    recordings.notifyDataSetChanged();
                    updateCustom();
                }
            }
        };
        IntentFilter ff = new IntentFilter();
        ff.addAction(PAUSE_BUTTON);
        ff.addAction(STOP_BUTTON);
        registerReceiver(receiver, ff);

        screenreceiver = new ScreenReceiver();
        screenreceiver.registerReceiver(this);

        handler = new Handler();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab_panel = findViewById(R.id.fab_panel);

        fab_stop = findViewById(R.id.fab_stop);
        fab_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordings.playerStop();
                updateCustom();
            }
        });

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recordings.playing == PLAYING_CUSTOM) {
                    if (recordings.player.isEnd()) {
                        playCustom();
                    } else {
                        recordings.playerPause();
                        updateCustom();
                    }
                } else {
                    CustomDialog dialog = new CustomDialog();
                    Bundle args = createCustomArgs();
                    dialog.setArguments(args);
                    dialog.show(getSupportFragmentManager(), "");
                }
            }
        });

        recordings = new Recordings(this);

        list = (ListView) findViewById(R.id.list);
        list.setOnScrollListener(this);
        list.setAdapter(recordings);
        list.setEmptyView(findViewById(R.id.empty_list));

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);

        if (shared.getBoolean(MainApplication.PREFERENCE_CALL, false)) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(pscl, PhoneStateListener.LISTEN_CALL_STATE);
        }

        SeekBar sbg = (SeekBar) findViewById(R.id.volume_bg);
        sbg.setProgress((int) (MainApplication.getFlatBgVol(this) * 100));
        sbg.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = progress / 100f;
                SharedPreferences.Editor edit = shared.edit();
                edit.putFloat(MainApplication.PREFERENCE_BG, vol);
                edit.apply();
                if (recordings.player != null) {
                    recordings.player.setFlatBgVol(vol);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        SeekBar svc = (SeekBar) findViewById(R.id.volume_vc);
        svc.setProgress((int) (MainApplication.getFlatVcVol(this) * 100));
        svc.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float vol = progress / 100f;
                SharedPreferences.Editor edit = shared.edit();
                edit.putFloat(MainApplication.PREFERENCE_VC, vol);
                edit.apply();
                if (recordings.player != null) {
                    recordings.player.setFlatVcVol(vol);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        updateCustom();
    }

    // load recordings
    void load() {
        recordings.scan();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (id == R.id.action_test) {
            SoundTestDialog dialog = new SoundTestDialog();
            dialog.show(getSupportFragmentManager(), "");
            return true;
        }
        if (id == R.id.action_open) {
            choicer = new OpenChoicer(OpenFileDialog.DIALOG_TYPE.FILE_DIALOG, true) {
                @Override
                public void onResult(Uri uri) {
                    openDialog(uri);
                }
            };
            choicer.setPermissionsDialog(this, PERMISSIONS, RESULT_FILE);
            choicer.setStorageAccessFramework(this, RESULT_FILE);
            choicer.show(null);
        }
        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        load();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RESULT_FILE:
                choicer.onRequestPermissionsResult(permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_FILE:
                choicer.onActivityResult(resultCode, data);
                break;
        }
    }

    void openDialog(Uri u) {
        OpenDialog dialog = new OpenDialog();
        Bundle args = new Bundle();
        args.putParcelable("url", u);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.scrollState = scrollState;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        handler.post(new Runnable() {
            @Override
            public void run() {
                list.smoothScrollToPosition(recordings.selected);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recordings.close();

        if (pscl != null) {
            TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(pscl, PhoneStateListener.LISTEN_NONE);
            pscl = null;
        }
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        if (screenreceiver != null) {
            unregisterReceiver(screenreceiver);
            screenreceiver = null;
        }
    }

    @Override
    public void onStart() {
        try { // onCreateDialog exceptions
            super.onStart();
        } catch (RuntimeException e) {
            Error(this, e);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (recordings.player != null)
            moveTaskToBack(true);
        else
            super.onBackPressed();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dialog == null)
            return;

        if (dialog instanceof CustomDialog.Result) {
            CustomDialog.Result r = (CustomDialog.Result) dialog;

            final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor edit = shared.edit();
            edit.putInt(MainApplication.CUSTOM_BACKGROUND, r.background.ordinal());
            edit.putLong(MainApplication.CUSTOM_DURATION, r.duration);
            edit.putBoolean(MainApplication.CUSTOM_BEAT, r.beats);
            edit.putFloat(MainApplication.CUSTOM_BASE, r.base);
            edit.putFloat(MainApplication.CUSTOM_BEATSTART, r.beatStart);
            edit.putFloat(MainApplication.CUSTOM_BEATEND, r.beatEnd);
            edit.commit();

            playCustom();
        }

        if (dialog instanceof OpenDialog.Result) {
            OpenDialog.Result r = (OpenDialog.Result) dialog;
            if (r.done)
                playCustom(r.pr);
        }
    }

    Bundle createCustomArgs() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        Bundle args = new Bundle();
        args.putLong("duration", shared.getLong(MainApplication.CUSTOM_DURATION, 60 * 60 * 1000 + 20 * 60 * 1000));
        args.putInt("background", shared.getInt(MainApplication.CUSTOM_BACKGROUND, SoundLoop.WHITE_NOISE.ordinal()));
        args.putBoolean("beats", shared.getBoolean(MainApplication.CUSTOM_BEAT, true));
        args.putFloat("base", getFloat(shared, MainApplication.CUSTOM_BASE, (float) Note.A_FREQ));
        args.putFloat("start", getFloat(shared, MainApplication.CUSTOM_BEATSTART, 20f));
        args.putFloat("end", getFloat(shared, MainApplication.CUSTOM_BEATEND, 60f));
        return args;
    }

    void playCustom() {
        Bundle args = createCustomArgs();

        Program pr = new Program(getString(R.string.group_mediation));
        Period p = new Period((int) (args.getLong("duration") / 1000), SoundLoop.values()[args.getInt("background")], BeatsPlayer.DEFAULT_BG_VOL);
        if (args.getBoolean("beats"))
            p.addVoice(new BinauralBeatVoice(args.getFloat("start"), args.getFloat("end"), BeatsPlayer.DEFAULT_VOLUME, args.getFloat("base")));
        pr.addPeriod(p);

        playCustom(pr);
    }

    void playCustom(Program pr) {
        recordings.playerStop();
        recordings.notifyDataSetChanged(); // update preset views

        recordings.playerPlay(fab_panel, pr, PLAYING_CUSTOM);

        updateCustom();
    }

    void updateCustom() {
        if (recordings.player == null || recordings.playing != PLAYING_CUSTOM) {
            fab_panel.setVisibility(View.GONE);
            fab.setImageResource(R.drawable.ic_access_alarm_black_24dp);
        } else {
            fab_panel.setVisibility(View.VISIBLE);
            if (recordings.player.isPlaying())
                fab.setImageResource(R.drawable.ic_pause_black_24dp);
            else
                fab.setImageResource(R.drawable.ic_play_arrow_black_24dp);
        }
    }

    public static void Error(Context context, Throwable e) {
        Throwable t = e;
        while (t.getCause() != null)
            t = t.getCause();
        String msg = e.getMessage();
        if (msg == null || msg.isEmpty())
            msg = t.getClass().getSimpleName();
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}
