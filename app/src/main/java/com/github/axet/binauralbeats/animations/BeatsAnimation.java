package com.github.axet.binauralbeats.animations;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.ListView;

import com.github.axet.androidlibrary.animations.MarginAnimation;
import com.github.axet.binauralbeats.R;

public class BeatsAnimation extends MarginAnimation {
    ListView list;

    View convertView;
    View bottom_ctr;
    View bottom_s;
    View compact_s;

    boolean partial;
    Handler handler;
    boolean playing;

    // true if this animation was started simultaneously with expand animation.
    boolean collapse_multi = false;

    // if we have two concurrent animations on the same listview
    // the only one 'expand' should have control of showChild function.
    static BeatsAnimation atomicExpander;

    public static void apply(final ListView list, final View v, final boolean expand, boolean animate, final boolean playing) {
        apply(new LateCreator() {
            @Override
            public MarginAnimation create() {
                BeatsAnimation a = new BeatsAnimation(list, v, expand, playing);
                if (expand)
                    atomicExpander = a;
                return a;
            }
        }, v, expand, animate);
    }

    public BeatsAnimation(ListView list, View v, boolean expand, boolean playing) {
        super(v.findViewById(R.id.recording_player), expand);

        handler = new Handler();

        this.convertView = v;
        this.list = list;
        this.playing = playing;

        bottom_ctr = v.findViewById(R.id.beats_controls);
        bottom_s = v.findViewById(R.id.beats_expand);
        compact_s = v.findViewById(R.id.beats_compact);
    }

    public void init() {
        super.init();

        bottom_ctr.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        bottom_s.setVisibility(expand ? View.INVISIBLE : View.VISIBLE);
        compact_s.setVisibility(expand ? View.VISIBLE : View.INVISIBLE);

        {
            final int paddedTop = list.getListPaddingTop();
            final int paddedBottom = list.getHeight() - list.getListPaddingTop() - list.getListPaddingBottom();

            partial = false;

            partial |= convertView.getTop() < paddedTop;
            partial |= convertView.getBottom() > paddedBottom;
        }
    }

    @Override
    public void calc(final float i, Transformation t) {
        super.calc(i, t);

        float ii = expand ? i : 1 - i;
        float k = expand ? 1 - i : i;

        if (Build.VERSION.SDK_INT >= 11) {
            compact_s.setRotation(180 * ii);
            if (playing)
                bottom_ctr.setAlpha(k);
            bottom_s.setRotation(-180 + 180 * ii);
        }

        // ViewGroup will crash on null pointer without this post pone.
        // seems like some views are removed by RecyvingView when they
        // gone off screen.
        if (Build.VERSION.SDK_INT >= 19) {
            collapse_multi |= !expand && atomicExpander != null && !atomicExpander.hasEnded();
            if (collapse_multi) {
                // do not showChild;
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showChild(i);
                    }
                });
            }
        }
    }

    @TargetApi(19)
    void showChild(float i) {
        final int paddedTop = list.getListPaddingTop();
        final int paddedBottom = list.getHeight() - list.getListPaddingTop() - list.getListPaddingBottom();

        if (convertView.getTop() < paddedTop) {
            int off = convertView.getTop() - paddedTop;
            if (partial)
                off = (int) (off * i);
            list.scrollListBy(off);
        }

        if (convertView.getBottom() > paddedBottom) {
            int off = convertView.getBottom() - paddedBottom;
            if (partial)
                off = (int) (off * i);
            list.scrollListBy(off);
        }
    }

    @Override
    public void restore() {
        super.restore();
        if (Build.VERSION.SDK_INT >= 11) {
            bottom_ctr.setAlpha(1);
            bottom_s.setRotation(0);
            compact_s.setRotation(0);
        }
    }

    @Override
    public void end() {
        super.end();
        if (expand) {
            bottom_ctr.setVisibility(View.INVISIBLE);
        } else {
            bottom_ctr.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        }
        bottom_s.setVisibility(expand ? View.VISIBLE : View.INVISIBLE);
        compact_s.setVisibility(expand ? View.INVISIBLE : View.VISIBLE);
        view.setVisibility(expand ? View.VISIBLE : View.GONE);
    }
}