package com.github.axet.binauralbeats.beats;

import android.content.Context;
import android.util.Log;

import com.github.axet.androidlibrary.sound.AudioTrack;
import com.github.axet.binauralbeats.app.Sound;

public class WhiteNoise implements Runnable {
    private static final String TAG = WhiteNoise.class.getSimpleName();

    public final Object lock = new Object();

    AudioTrack track;

    Thread thread;

    AudioTrack.AudioBuffer buf;

    public State state = new State();

    public static class State {
        public long dur;
        public long durSamples; // 1 infiinte with dur == 0
        public long samplesNum;

        public float fadeStart; // fade start vol
        public long fadeStartSamples; // fade start samles count
        public long fadeInEnd; // fade start last sample number

        public float fadeEnd; // fade end vol
        public long fadeEndSamples; // fade end samples count
        public long fadeOutStart; // fade end first sample number

        public State() {
        }

        public State(State state) {
            dur = state.dur;
            durSamples = state.durSamples;
            samplesNum = state.samplesNum;

            fadeStart = state.fadeStart;
            fadeStartSamples = state.fadeStartSamples;
            fadeInEnd = state.fadeInEnd;

            fadeEnd = state.fadeEnd;
            fadeEndSamples = state.fadeEndSamples;
            fadeOutStart = state.fadeOutStart;
        }

        public void setDur(int hz, long d) {
            dur = d;
            durSamples = dur * hz / 1000;
        }

        public void setFadeStart(float vol, int hz, int fadeStart) {
            this.fadeStart = vol;
            setFadeStart(hz, fadeStart);
        }

        public void setFadeStart(int hz, int fadeStart) {
            fadeStartSamples = fadeStart * hz / 1000;
            fadeInEnd = fadeStartSamples;
        }

        public void setFadeEnd(float vol, int hz, int fadeEnd) {
            this.fadeEnd = vol;
            setFadeEnd(hz, fadeEnd);
        }

        public void setFadeEnd(int hz, int fadeEnd) {
            fadeEndSamples = fadeEnd * hz / 1000;
            fadeOutStart = durSamples - fadeEndSamples;
        }

        public void setInfinite() {
            dur = 0;
            durSamples = 1; // infinite with dur == 0
        }

        public boolean isInfinite() {
            return dur == 0 && durSamples == 1;
        }
    }

    public WhiteNoise(Context context) {
        int rate = Sound.getAudioRate(context);
        if (rate == -1)
            throw new RuntimeException("unable to initiazlia audio params"); // unsupported rate or channel numbers
        buf = new AudioTrack.AudioBuffer(rate, Sound.DEFAULT_CHANNELS, Sound.DEFAULT_AUDIOFORMAT);
        track = AudioTrack.create(Sound.DEFAULT_STREAM, Sound.DEFAULT_USAGE, Sound.DEFAULT_TYPE, buf, buf.len * Sound.DEFAULT_BUF);
    }

    public void play(long dur, float vol, int fadeStart, int fadeEnd) {
        State state = new State();
        state.setDur(buf.hz, dur);
        state.setFadeStart(0f, buf.hz, fadeStart);

        if (dur > 0) {
            state.setFadeEnd(0f, buf.hz, fadeEnd);
        }
        synchronized (lock) {
            this.state = state;
            lock.notifyAll();
            setVolume(vol);
        }
    }

    public boolean isPlaying() {
        return thread != null;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, "WhiteNoise");
            thread.start();
        }
    }

    public void pause() {
        track.pause();
        stopThread();
    }

    public void stop() {
        track.stop();
        stopThread();
    }

    void stopThread() {
        if (thread != null) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
        thread = null;
    }

    public void close() {
        stopThread();
        if (track != null) {
            track.release();
            track = null;
        }
    }

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        track.play();

        Thread t = Thread.currentThread();
        State state;
        while (!t.isInterrupted()) {
            synchronized (lock) {
                state = this.state; // update every call, since we can skip unplayed white noise from previous session
                if (state.dur > 0) {
                    if (state.samplesNum >= state.durSamples) {
                        if (!next(state)) {
                            try {
                                lock.wait();
                            } catch (InterruptedException ignore) {
                                return;
                            }
                        }
                        state = this.state;
                    }
                }
            }

            for (int i = 0; i < buf.len; i += 2) {
                float vol = 1f;
                if (state.samplesNum < state.fadeInEnd) {
                    float flat = state.samplesNum / (float) state.fadeStartSamples;
                    vol = state.fadeStart + Sound.log1(flat) * (1f - state.fadeStart);
                }
                if (state.dur > 0) {
                    if (state.samplesNum > state.fadeOutStart) {
                        float flat = (state.durSamples - state.samplesNum) / (float) state.fadeEndSamples;
                        if (flat < 0) // // when samples buffer is bigger then fadeOut mute the rest
                            flat = 0;
                        vol = state.fadeEnd + Sound.log1(flat) * (1f - state.fadeEnd);
                    }
                }
                buf.write(i, (short) (vol * Math.random() * Short.MAX_VALUE), (short) (vol * Math.random() * Short.MAX_VALUE));
                state.samplesNum++;
            }
            buf.reset();

            int out = track.write(buf);
            if (out < 0)
                return;
            int diff = buf.len - buf.pos; // did we write less? track on pause.
            state.samplesNum -= diff; // reduce current samplesNum by rest.
        }
    }

    public void setVolume(float vol) {
        track.setStereoVolume(vol, vol);
    }

    public boolean next(State state) {
        return state != this.state;
    }
}
