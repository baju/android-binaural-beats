package com.github.axet.binauralbeats.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RemoteViews;

import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.activities.MainActivity;
import com.github.axet.binauralbeats.app.MainApplication;
import com.github.axet.binauralbeats.app.Sound;

/**
 * RecordingActivity more likly to be removed from memory when paused then service. Notification button
 * does not handle getActvity without unlocking screen. The only option is to have Service.
 * <p/>
 * So, lets have it.
 * <p/>
 * Maybe later this class will be converted for fully feature recording service with recording thread.
 */
public class BeatsService extends Service {
    public static final String TAG = BeatsService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static String SHOW_ACTIVITY = BeatsService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = BeatsService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = BeatsService.class.getCanonicalName() + ".STOP_BUTTON";

    RecordingReceiver receiver;

    String name = "";
    String time = "";
    boolean playing;
    boolean end;
    Sound sound;
    NotificationCompat.Builder builder;
    MediaSessionCompat ms;
    PendingIntent pause;
    PendingIntent stop;

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            MediaButtonReceiver.handleIntent(ms, intent);
        }
    }

    public static void startService(Context context, String name, String time, boolean playing, boolean end) {
        context.startService(new Intent(context, BeatsService.class)
                .putExtra("name", name)
                .putExtra("time", time)
                .putExtra("playing", playing)
                .putExtra("end", end)
        );
    }

    public static void stopService(Context context) {
        context.stopService(new Intent(context, BeatsService.class));
    }

    public BeatsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        pause = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(PAUSE_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        stop = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(STOP_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        sound = new Sound(this);

        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);
        registerReceiver(receiver, filter);

        builder = new NotificationCompat.Builder(this);

        startForeground(NOTIFICATION_RECORDING_ICON, buildNotification());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String a = intent.getAction();
            if (a == null) {
                name = intent.getStringExtra("name");
                time = intent.getStringExtra("time");
                playing = intent.getBooleanExtra("playing", false);
                end = intent.getBooleanExtra("end", false);

                if (playing)
                    sound.silent();
                else
                    sound.unsilent();

                showNotificationAlarm(true);
            } else if (a.equals(PAUSE_BUTTON)) {
                Intent i = new Intent(MainActivity.PAUSE_BUTTON);
                sendBroadcast(i);
            } else if (a.equals(STOP_BUTTON)) {
                Intent i = new Intent(MainActivity.STOP_BUTTON);
                sendBroadcast(i);
            } else if (a.equals(SHOW_ACTIVITY)) {
                MainActivity.startActivity(this);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class Binder extends android.os.Binder {
        public BeatsService getService() {
            return BeatsService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");

        sound.unsilent();

        showNotificationAlarm(false);

        unregisterReceiver(receiver);
    }

    Notification buildNotification() {
        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews view = new RemoteViews(getPackageName(), MainApplication.getTheme(getBaseContext(),
                R.layout.notifictaion_recording_light,
                R.layout.notifictaion_recording_dark));

        boolean pause = !playing && !end;
        String title = name;

        if (title.isEmpty()) {
            title = "App Killed by Android";
            time = "(tap to restart)";
            view.setViewVisibility(R.id.notification_pause, View.GONE);
            view.setViewVisibility(R.id.notification_stop, View.GONE);
            builder.setOngoing(false);
            headset(false, playing);
        } else {
            view.setViewVisibility(R.id.notification_pause, View.VISIBLE);
            view.setViewVisibility(R.id.notification_stop, View.VISIBLE);
            title += (pause ? getString(R.string.pause_notify) : "…");
            builder.setOngoing(!end); // ongoing until end
            headset(true, playing);
        }

        view.setOnClickPendingIntent(R.id.status_bar_latest_event_content, main);
        view.setTextViewText(R.id.notification_title, title);
        view.setTextViewText(R.id.notification_text, time);
        view.setOnClickPendingIntent(R.id.notification_pause, this.pause);
        view.setOnClickPendingIntent(R.id.notification_stop, this.stop);
        view.setImageViewResource(R.id.notification_pause, !playing ? R.drawable.ic_play_arrow_black_24dp : R.drawable.ic_pause_black_24dp);

        builder.setOnlyAlertOnce(true)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentText(time)
                .setContent(view);

        if (Build.VERSION.SDK_INT < 11) {
            builder.setContentIntent(main);
        }

        if (Build.VERSION.SDK_INT >= 21)
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        return builder.build();
    }

    // alarm dismiss button
    public void showNotificationAlarm(boolean show) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (!show) {
            notificationManager.cancel(NOTIFICATION_RECORDING_ICON);
        } else {
            notificationManager.notify(NOTIFICATION_RECORDING_ICON, buildNotification());
        }
    }

    void headset(boolean b, boolean playing) {
        if (b) {
            if (ms == null)
                ms = new MediaSessionCompat(this, TAG, new ComponentName(this, RecordingReceiver.class), null);
            ms.setCallback(new MediaSessionCompat.Callback() {
                @Override
                public void onPlay() {
                    pause();
                }

                @Override
                public void onPause() {
                    pause();
                }

                @Override
                public void onStop() {
                    try {
                        stop.send();
                    } catch (PendingIntent.CanceledException e) {
                        Log.d(TAG, "pause canceled", e);
                    }
                }
            });
            PlaybackStateCompat state = new PlaybackStateCompat.Builder()
                    .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_STOP)
                    .setState(playing ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED, 0, 1)
                    .build();
            ms.setPlaybackState(state);
            ms.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            ms.setActive(true);
        } else {
            if (ms != null) {
                ms.release();
                ms = null;
            }
        }
    }

    void pause() {
        try {
            pause.send();
        } catch (PendingIntent.CanceledException e) {
            Log.d(TAG, "pause canceled", e);
        }
    }
}
